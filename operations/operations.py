from abc import ABC, abstractmethod

class Operation (ABC):
    """
    Classe abstraite principale

    """
    def __init__(self):
        pass

    @abstractmethod
    def execute(self, table):
        pass