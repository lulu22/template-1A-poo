from operations.transformers.transformers import Transformers
from operations.estimators.estimators import Estimators
from operations.transformers.mapexport import CartoPlot
from operations.transformers.csvexport import Csvexport
from operations.transformers.selection_var import SelectionVar


class Pipeline:
    """
    Classe qui permet d'appliquer à des données une autre classe

    methods:
    -------------

    run

    """

    def __init__(self, table, etape):
        """
        Constructeur

        parameters:
        ------------

       table: table de données 
            Table
        
        etape:
            la classe que l'on veut appliquer à notre table

        """
        self.etape = etape
        self.table = table

    def run(self):
        """
        fonction qui permet d'appliquer à notre table une classe

        Returns:
        ------------

        table 

        """
        if isinstance(self.etape, Csvexport):
            self.etape.transform(self.table)
        elif isinstance(self.etape, CartoPlot):
            cp = CartoPlot()
            # d = {}
            # d['Betagni'] = 1
            # fig = cp.plot_reg_map(data=self.table)
            # fig.show()
            # fig.savefig('./data/export/regions.test.jpg')
            d = {}
            for i in range(1, 96):
                d[str(i)] = i
            del (d['69'])
            d['69D'] = 69
            d['69M'] = 69
            d['2A'] = 20
            d['2B'] = 20.5
            fig = cp.plot_dep_map(data=self.table, x_lim=(-6, 10), y_lim=(41, 52))
            fig.show()
            fig.savefig('./data/export/hosp_7j_departements.jpg')
        elif isinstance(self.etape, Transformers):
            return self.etape.transform(self.table)
        elif isinstance(self.etape, SelectionVar):
            return self.etape.transform(self.table)
        elif isinstance(self.etape, Estimators):
            return self.etape.fit(self.table)
        else:
            return self.etape.transform(self.table)
