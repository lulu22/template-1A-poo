class Table:
    """
    
    Permet de définir au mieux les tables pour les utiliser dans le reste du projet


    methods:
    -------------

    creation_table: crée une table composée d'une entête et d'un corp (body)
    colonne: renvoie la colonne qui correspond à la variable que l'on a choisi
    select_ligne: permet de selectionner la ligne de la table que l'on souhaite
    type_col: change le type des variables

    
    """
    def __init__(self, table):

        """
        Constructeur

        attributes:
        ------------
        entete: première ligne de notre table
            lst
        corps: le reste de notre table
            lst

        parameters:
        ------------
        table: table de données
            Table

        """
        self.entete = table[0]
        self.corps = table[1:len(table)]

    def creation_table(self):
        """
        Crée une table pour résumer les données

        returns:
        ----------
        liste de listes
        
        """
        return [self.entete, self.corps]

    def colonne(self,table, variable):
        """
        Renvoie la colonne qui correspond à la variable que l'on a choisi

        parameters:
        ------------
        table: table de laquelle on extrait la colonne
            Table

        variable: nom de la colonne que l'on a choisi
            str

        returns:
        ----------
        col: colonne 
            list
        
        """
        col = []
        index = table.entete.index(variable)
        for ligne in table.corps :
            col.append(ligne[index])
        return col

    def select_ligne(self, table, var_condition, condition):
        """
        Permet de selectionner les lignes de la table que l'on souhaite garder selon une condition

        parameters:
        ------------
        table: table de laquelle on extrait les lignes
            Table

        var_condition: variable sur laquelle on met une condition
            str
        
        condition: condition que l'on applique à la variable
            str

        returns:
        ----------
        table_finale: 
            Table

        """
        table_finale = [table.entete]
        index_colonne = table.entete.index(var_condition)
        for ligne in table.corps :
            if ligne[index_colonne] == condition :
                table_finale.append(ligne)
        return table_finale

    def type_col(self, variable):

        """
        Fonction qui permet de convertir une colonne dont les élements sont tous des int dans des str en int
       
        parameters:
        ------------
        variable: nom de la variable 
            str

        returns:
        --------
        liste_finale: 
            list
        
        """
        #tous_nombre = all([self.corps[k][index_var].isnumeric() for k in range(len(self.corps))])
        liste_finale = [variable]
        tous_nombre = all([elmt.isnumeric() for elmt in self.corps])
        if tous_nombre :
            for elmt in self.corps :
                liste_finale.append(int(elmt))
        return liste_finale













