import math


class Outils :
    """
    Classe qui regroupe des méthodes qui nous sont utiles dans les 2 principales classes abstraites (Transformers et Estimators)

    method:
    -------------
    moyenne

    ecart_type

    """
    def __init__(self):
        """
        Constructeur

        """
        pass

    def moyenne(self, lst):
        """
        Fonction qui permet de calculer la moyenne d'une liste

        Parameters:
        ------------

        lst: liste sur laquelle on veut calculer la moyenne
            list

        Returns:
        -------
        float

        """
        taille = len(lst)
        somme = sum(lst)
        return round(somme / taille, 2)

    def ecart_type(self, lst):
        """
        Fonction qui permet de calculer l'écart type d'une liste

        Parameters:
        ------------

        lst: liste sur laquelle on veut calculer l'écart type
            list

        Returns:
        -------
        float

        """
        somme = 0
        taille = len(lst)
        for elmt in lst :
            somme += (elmt - self.moyenne(lst))**2
        return round(math.sqrt(somme / taille), 2)



