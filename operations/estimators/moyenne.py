from operations.estimators.estimators import Estimators

class Moyenne(Estimators):

    """
    Renvoie la moyenne des variables selectionnées
    hérite de la classe abstraite Estimators

    attributes:
    -------------

    methods:
    -------------

    moyenne: calcule la moyenne 
    
    """
    def __init__(self):
        """
        Constructeur
        """
        Estimators.__init__(self)


    def fit(self, table):
        """
        Méthode qui renvoie la moyenne de la table en entrée

        parameters:
        ------------

        table: liste selectionnée
            list

        returns:
        ---------
        float

        Examples:
        ---------
        >>> Moyenne().fit([1,2,3])
        2

        """

        taille = len(table.corps)
        somme = sum(table.corps)
        return round(somme / taille, 2)

