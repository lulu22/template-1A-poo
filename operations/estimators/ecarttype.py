from operations.estimators.estimators import Estimators
from operations.estimators.moyenne import Moyenne
import math

class Ecarttype(Estimators):

    """
    Renvoie l'ecart-type des variables selectionnées
    hérite de la classe abstraite Estimators

    Attributes:
    -------------

    Method:
    -------------

    fit: calcule l'ecart-type

    """

    def __init__(self):

        """
        Constructeur, hérite de celui de la classe Estimators
        """

        Estimators.__init__(self)

    def fit(self, table):

        """
        Méthode qui Renvoie l'ecart-type de la table (souvent une liste) en entrée

        Parameters:
        ------------

        table: liste selectionée
            list

        Returns:
        --------

        float

        """

        somme = 0
        taille = len(table)
        for elmt in table :
            somme = somme + (elmt - Moyenne.fit(table))**2
        return round(math.sqrt(somme / taille), 2)