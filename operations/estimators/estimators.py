from abc import abstractmethod
from operations.operations import Operation

class Estimators(Operation):
    """
    Classe qui hérite de la classe abstraite Operation

    """
    def __init__(self):
        Operation.__init__(self)

    @abstractmethod
    def fit(self, table):
        pass

    def execute(self, table):
        self.fit(table)