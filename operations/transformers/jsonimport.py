import json
from operations.transformers.transformers import Transformers

class Jsonimport(Transformers):

    """
    Importe les fichiers json correspondant au calendrier et aux académies
    hérite de la classe abstraite Transformers


    """

    def __init__(self, fichier):

        """
        Constructeur
    
        parameters:
        ------------

        fichier : fichier à importer 
            str
    
        """
        Transformers.__init__(self)
        self.fichier = fichier

    def transform(self, table):

        """
        Fonction qui permet d'importer un fichier json

        Parameters:
        ------------

        table: table dans laquelle on veut stocker le fichier json importé, on met souvent une liste vide []
            list

        """

        with open(file=self.fichier) as json_file :
            table = json.load(json_file)
            return table

