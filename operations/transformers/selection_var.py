from operations.transformers.transformers import Transformers

class SelectionVar(Transformers):

    """
    Selectionne des variables voulues
    Hérite de la classe abstraite Transformers

    methods:
    -------------

    transform:

    """
    def __init__(self, variable):

        """
        Constructeur

        parameters:
        ------------

        variable: variable à selectionner
            str
            
        """
        Transformers.__init__(self)
        self.variable = variable

    def transform(self, table):

        """
        Fonction qui permet de sélectionner la colonne correspondant à la variable dans la table

        Parameters:
        ------------

        table: table d'entrée
            Table

        Returns:
        -------

        table_finale: table qui ne contient plus que la colonne souhaité
            Table, ou list
        """

        table_finale = [self.variable]
        index_var = table.entete.index(self.variable)
        for ligne in table.corps :
            table_finale.append(ligne[index_var])
        return table_finale

