from operations.transformers.transformers import Transformers
import datetime

class Fenetrage(Transformers):
    """
    Selectionne des données suivant une période 
    Hérite de la classe abstraite Transformers

    methods:
    -------------

    transform:

    """
    def __init__(self, date_debut, date_fin):

        """
        Constructeur

        Parameters:
        ------------
        date_debut: date à laquelle commence la selection des données
            datetime.datetime

        date_fin: date à laquelle finit la selection des données 
            datetime.datetime

        """

        Transformers.__init__(self)
        # vérification que la période est comprise dans les données disponibles
        if (date_debut >= datetime.datetime(2020, 3, 18)) and (date_fin <= datetime.datetime(2021, 3, 3)) and (date_fin >= date_debut):
            self.date_debut = date_debut
            self.date_fin = date_fin
        else:
            print("Les données sur votre période ne sont pas disponibles. Entrez une période entre [18/03/2020, 03/03/2021]")


    def transform(self, table):
        """
        Fonction qui sélectionne seulement les lignes qui appartiennent à la période souhaitée

        Parameters:
        ------------

        table: table sur laquelle on applique le fenêtrage
            Table

        Returns:
        --------

        table_fenetree: table avec seulement les lignes qui appartiennent à la période souhaitée
            Table

        """

        table_fenetree = [table.entete]
        index_jour = table.entete.index("jour") #on regarde dans la première liste, dans laquelle colonne est "jour"
        for ligne in table.corps :
            if self.date_debut <= datetime.datetime.strptime(ligne[index_jour], "%Y-%m-%d") <= self.date_fin:
                table_fenetree.append(ligne)
        return table_fenetree