from operations.transformers.transformers import Transformers
import csv


class Csvexport(Transformers):
    
    """
    Renvoie les résulats des pipelines (des questions) sous la forme d'un csv
    hérite de la classe abstraite Transformers

    method:
    -------------

    transform:

    """

    def __init__(self, chemin, encoding="ISO-8859-1"):

        """
        Constructeur

        parameters:
        ------------

        chemin: chemin du dossier
            str

        encoding: encodage du fichier
            str
    
        """
        Transformers.__init__(self)
        self.chemin = chemin
        self.encoding = encoding

    def transform(self, table):

        """
        Fonction qui permet d'écrire dans un fichier, et donc d'exporter un fichier

        parameters:
        ------------

        table: table que l'on veut exporter
            Table

        """

        with open(self.chemin, "w", newline="") as f:
            ecriture = csv.writer(f, delimiter=" ")
            for ligne in table:
                ecriture.writerow(ligne)
