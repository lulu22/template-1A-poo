from abc import abstractmethod
from operations.operations import Operation


class Transformers(Operation):
    """
    Classe qui hérite de la classe abstraite Operation

    """
    def __init__(self):
        Operation.__init__(self)

    @abstractmethod
    def transform(self, table):
        pass

    def execute(self, table):
        self.transform(table)
