from operations.transformers.transformers import Transformers

class Jointure(Transformers):
    """
    Joint différentes tables 
    Hérite de la classe abstraite Transformers

    method:
    -------------

    dictionnaire: convertit les tables en dictionnaires

    transform:

    """

    def __init__(self, tableajoindre, var):
         """
        Constructeur

        parameters:
        ------------
        tableajoindre: table a joindre 
            Table

        var: nom de la variable sur laquelle on joint les tables (clé primaire)
            str

        """
        Transformers.__init__(self)
        self.tableajoindre = tableajoindre
        self.var = var
    
#on transforme nos tables en dictionnaires
    def dictionnaire(self, table):

        """
        Transforme les tables récupérées sous forme de dictionnaires 

        parameters:
        ------------
        table: Table concernée 
            table 

        returns:
        ----------
        dico_principal: dictionnaire correspondant à la table
            dict
        
        """
        dico_principal = {}
        dico_valeurs = {}
        indice_var_table = table.entete.index(self.var)
        for obs_var in table.colonne(table, self.var) :
            for ligne in table.corps :
                for variable in table.entete :
                    indice_variable = table.entete.index(variable) #donne num colonne de chaque variable
                    dico_valeurs[variable] = ligne[indice_variable] #prend la ligne et la valeur de la variable que je veux
            dico_principal[obs_var] = dico_valeurs
        del dico_valeurs[self.var]
        return dico_principal

#on joint les deux dictionnaires
    def transform(self,table):

        """
        Joint les deux dictionnaires  

        parameters:
        ------------
        table: Table concernée 
            Table

        returns:
        ----------
        [entete, liste_clefs + liste_val] : liste des listes avec les valeurs des deux tables
            list 

        """
        dico1 = Jointure(self.tableajoindre, self.var).dictionnaire(table)
        dico2 = Jointure(self.tableajoindre, self.var).dictionnaire(self.tableajoindre)
        dico1.update(dico2)
        #return dico1
#il faut à présent convertir ce dictionnaire en listes
        liste_val = []
        liste_clefs = []
        i = self.tableajoindre.entete.index(self.var) #on récupère l'indice de la variable sur laquelle on a fait la jointure
        del self.tableajoindre.entete[i] #on supprime cette variable dans le deuxième tableau pour qu'elle ne soit pas deux fois dans le tableau final
        entete = table.entete + self.tableajoindre.entete 
        for c in dico1.keys():
            liste_clefs.append(c)
            for v in dico1.values():
                liste_clefs.append(v)
        return [entete, liste_clefs + liste_val]
