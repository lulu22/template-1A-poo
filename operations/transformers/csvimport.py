from operations.transformers.transformers import Transformers
import csv

class Csvimport(Transformers):

    """
    Importe les tables de données au format Csv du Covid
    hérite de la classe abstraite Transformers

    method:
    -------------
    transform:

    """

    def __init__(self, fichier, encoding="ISO-8859-1", delimiter=";"):
        """
        Constructeur

        Parameters:
        ------------

        fichier : fichier à importer avec son chemin
            str

        encoding: encodage du fichier
            str

        delimiter: délimiteur utilisé dans le fichier csv
            str
    
        """

        Transformers.__init__(self)
        self.fichier = fichier
        self.encoding = encoding
        self.delimiter = delimiter

        
    def transform(self, table):

        """
        Fonction qui permet d'importer un fichier csv

        Parameters:
        ------------

        table: table dans laquelle on veut stocker le fichier csv importé, on met souvent une liste vide []
            list
        
        """
        with open(file=self.fichier, encoding=self.encoding) as csvfile:
            covidreader = csv.reader(csvfile, delimiter=self.delimiter)
            for ligne in covidreader:
                table.append(ligne)
        return table

