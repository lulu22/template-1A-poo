from operations.transformers.csvimport import Csvimport
from operations.transformers.csvexport import Csvexport
from operations.transformers.mapexport import CartoPlot
from operations.transformers.fenetrage import Fenetrage
from operations.transformers.jsonimport import Jsonimport
import datetime
from operations.transformers.selection_var import SelectionVar
from operations.table import Table
from operations.pipeline import Pipeline
from operations.estimators.moyenne import Moyenne


if __name__ == "__main__":

    # ------------------------------------------------------------#
    # Quel est le nombre total d’hospitalisations dues au Covid-19?
    # ------------------------------------------------------------#

    # importation des données, on transforme la table en objet Table
    data_hosp = Table(
        Csvimport("./data/import/donnees-hospitalieres-nouveaux-covid19-2021-03-03-17h03.csv").transform([]))
    # on sélectionne notre colonne nouvelles hospitalisations et on convertit cette colonne en int grâce à la
    # fonction type_col définit dans la classe Table
    pipe_selectvar = Table(Table(Pipeline(data_hosp, SelectionVar("incid_hosp")).run()).type_col("incid_hosp"))
    # On crée l'affichage
    affichage1 = [["Nombre total d'hospitalisations dûes au Covid-19"], [sum(pipe_selectvar.corps)]]
    # On exporte
    pipe_hosp = Pipeline(affichage1, Csvexport("./data/export/hosp_total_question1.csv"))
    pipe_hosp.run()  # 366449

    # -----------------------------------------------------------------------------------------------#
    # Combien de nouvelles hospitalisations ont eu lieu ces 7 derniers jours dans chaque département ?
    # -----------------------------------------------------------------------------------------------#

    # nous avons pris les 7 derniers jours de nos données, c'est à dire du 25/02/2021 au 3/03/2021 (car 28 jours en
    # Février)
    data_hosp7j = Table(
        Csvimport("./data/import/donnees-hospitalieres-nouveaux-covid19-2021-03-03-17h03.csv").transform([]))
    # on sélectionne les 7 derniers jours de nos données
    pipe_fenetrage = Table(
        (Pipeline(data_hosp7j, Fenetrage(datetime.datetime(2021, 2, 25), datetime.datetime(2021, 3, 3)))).run())
    # on crée un dictionnaire avec comme clé les départements et comme valeur la somme des hospitalisations des 7
    # derniers jours
    dico = {}
    dep = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12', '13', '14', '15', '16', '17', '18',
           '19', '21', '22', '23', '24', '25', '26', '27', '28', '29', '2A', '2B', '30', '31', '32', '33', '34', '35',
           '36', '37', '38', '39', '40', '41', '42', '43', '44', '45', '46', '47', '48', '49', '50', '51', '52', '53',
           '54', '55', '56', '57', '58', '59', '60', '61', '62', '63', '64', '65', '66', '67', '68', '69', '70', '71',
           '72', '73', '74', '75', '76', '77', '78', '79', '80', '81', '82', '83', '84', '85', '86', '87', '88', '89',
           '90', '91', '92', '93', '94', '95', '971', '972', '973', '974', '976']
    somme = 0
    for depar in dep:
        table = Table(pipe_fenetrage.select_ligne(pipe_fenetrage, "dep",
                                                  depar))  # on sélectionne les lignes où le département vaut depar
        for ligne in table.corps:
            somme += int(ligne[table.entete.index("incid_hosp")])
        dico[depar] = somme
        somme = 0  # on remet la somme à 0 car on passe à un nouveau département
    affichage2 = [["Département", "Nouvelles hospitalisations entre le 25/02/2021 au 3/03/2021"]]
    for cle, val in dico.items():
        affichage2.append([cle, val])

    # on exporte sous format csv
    pipe_newhosp7j = Pipeline(affichage2, Csvexport("./data/export/newhosp_7j_question2.csv"))
    pipe_newhosp7j.run()

    # on exporte sous format Carte
    pipe_newhosp7j_carte = Pipeline(dico, CartoPlot())
    pipe_newhosp7j_carte.run()

    # --------------------------------------------------------------------------------------------------#
    # Comment évolue la moyenne des nouvelles hospitalisations journalières de cette semaine par rapport
    # à celle de la semaine dernière ?
    # --------------------------------------------------------------------------------------------------#

    # La dernière semaine de nos données va du 25/02/2021 au 03/03/2021, on va comparer la moyenne des nouvelles
    # hospitalisations de cette semaine avec la semaine d'avant, c'est à dire la semaine du 18/02/2021 au 24/02/2021

    data_compare = Table(
        Csvimport("./data/import/donnees-hospitalieres-nouveaux-covid19-2021-03-03-17h03.csv").transform([]))
    # Fenêtrage pour la première période temporelle, c'est à dire pour la semaine du 18 février au 24 février
    pipe_compare_fenetrage1 = Table(
        (Pipeline(data_compare, Fenetrage(datetime.datetime(2021, 2, 18), datetime.datetime(2021, 2, 24)))).run())
    # Fenêtrage pour la deuxième période temporelle, c'est à dire pour la semaine du 25 février au 03 Mars
    pipe_compare_fenetrage2 = Table(
        (Pipeline(data_compare, Fenetrage(datetime.datetime(2021, 2, 25), datetime.datetime(2021, 3, 3)))).run())
    # on sélectionne la variable des nouvelles hospitalisations et on convertit en int
    pipe_compare_selectvar1 = Table(
        Table(Pipeline(pipe_compare_fenetrage1, SelectionVar("incid_hosp")).run()).type_col("incid_hosp"))
    # on sélectionne la variable des nouvelles hospitalisations et on convertit en int
    pipe_compare_selectvar2 = Table(
        Table(Pipeline(pipe_compare_fenetrage2, SelectionVar("incid_hosp")).run()).type_col("incid_hosp"))
    # on crée notre affichage
    affichage3 = [["Moyenne des nouvelles hospitalisations de la semaine du 25/02/2021 au 03/03/2021",
                   "Moyenne des nouvelles hospitalisations de la semaine du 18/02/2021 au 24/02/2021"],
                  [Moyenne().fit(pipe_compare_selectvar2), Moyenne().fit(pipe_compare_selectvar1)]]
    # on exporte
    pipe_compare = Pipeline(affichage3, Csvexport("./data/export/compare_hosp_question3.csv"))
    pipe_compare.run()

    # --------------------------------------------------------------------------------------------------#
    # Combien de nouvelles admissions en réanimation ont eu lieu pendant la semaine suivant les vacances
    # de la Toussaint de 2020?
    # --------------------------------------------------------------------------------------------------#

    #on importe nos données
    data_rea = Table(
        Csvimport("./data/import/donnees-hospitalieres-nouveaux-covid19-2021-03-03-17h03.csv").transform([]))
    #et on importe notre fichier json des vacances scolaires
    data_vacs = Jsonimport("./data/import/VacancesScolaires.json").transform([])

    #on cherche la date de fin des vacances de la Toussaint 2020
    liste_val = []
    liste_finale = []
    for dico in data_vacs["Calendrier"] :
        for cle, val in dico.items():
            if (cle, val) == ("Description","Vacances de la Toussaint"):
                liste_val.append(dico)
    for dic in liste_val:
        for key, value in dic.items():
            if (key, value) == ("annee_scolaire","2020-2021"):
                liste_finale.append(dic)
    for dico in liste_finale:
        date_fin = dico["Fin"]

    # on sélectionne les lignes qui correspondent à la semaine après les vacances de la Toussaint 2020, c'est-à-dire
    # du 02/11/2020 au 08/11/2020
    pipe_rea_fenetrage = Table(
        (Pipeline(data_rea, Fenetrage(datetime.datetime.strptime(date_fin, "%Y-%m-%d"), datetime.datetime.strptime(date_fin, "%Y-%m-%d")+ datetime.timedelta(6)))).run())


    #On sélectionne notre variable de nouvelles admissions en réanimation et on la met en int
    pipe_rea_selectvar = Table(
        Table(Pipeline(pipe_rea_fenetrage, SelectionVar("incid_rea")).run()).type_col("incid_rea"))

    #on crée notre affichage
    affichage4 = [["Nombre de nouvelles admissions en réanimation ont eu lieu pendant la semaine suivant les vacances "
                   "de la Toussaint de 2020"], [sum(pipe_rea_selectvar.corps)]]

    #on l'exporte
    pipe_rea = Pipeline(affichage4, Csvexport("./data/export/rea_aprestoussaint_question5.csv"))
    pipe_rea.run()
